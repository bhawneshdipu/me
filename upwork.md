I have good implementation knowledge in Web Development using Java, Python, PHP, and R implementing frameworks like Laravel, Spring-boot, Django/Flask and R shiny.
I have worked on MapR cluster with hands-on experience in Hive, spark, Sqoop, and Storm.I have developed Application based on API calls and Database integration with dashboard and web services using Python and  Java and D3.
I have worked on RDBMS databases like Oracle, Mysql, and PostgreSQL.I have worked on NoSql databased like MongoDB and Elasticsearch.
I have worked on Web servers like Apache Tomcat, Apache HTTP, and Apache nginx servers.

I am flexible with my working hours and am happy to work closely with any existing freelancers you work with. Building a relationship with my clients is of utmost importance to me so your satisfaction will be my priority.

I'm responsible and always punctual to deadlines.
Thank you!

